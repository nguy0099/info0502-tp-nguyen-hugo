package edu.info0502.tp1;
public class Livre extends Media {
    private String auteur;
    private String isbn;

    // Constructeur par défaut
    public Livre() {
        super(); 
        this.auteur = "Anonyme";
        this.isbn = "XXXX";
    }

    // Constructeur par initialisation
    public Livre(String titre, String cote, int note, String auteur, String isbn) {
        super(titre, cote, note); 
        this.auteur = auteur;
        this.isbn = isbn;
    }

    // Constructeur par copie
    public Livre(Livre other) {
        super(other); // Appelle le constructeur par copie de Media
        this.auteur = other.auteur;
        this.isbn = other.isbn;
    }

    // Getters et setters 
    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    // Masquage de la méthode toString
    @Override
    public String toString() {
        return "Livre{" +
                "titre='" + getTitre() + '\'' +
                ", cote=" + getCote() +
                ", note=" + getNote() +
                ", auteur='" + auteur + '\'' +
                ", isbn='" + isbn + '\'' +
                '}';
    }
}
