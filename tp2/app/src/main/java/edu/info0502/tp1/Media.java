package edu.info0502.tp1;
public class Media {

    private String titre;
    private StringBuffer cote;
    private int note;

    private static String nomMediatheque;

    public Media() {
        this.titre = "Default";
        this.cote = new StringBuffer("Default");
        this.note = 0;
    }


    public Media(String titre, String cote, int note) {
        this.titre = titre;
        this.cote = new StringBuffer(cote);
        this.note = note;
    }

    public Media(Media other) { 
        this.titre = other.titre;
        this.cote = new StringBuffer(other.cote); 
        this.note = other.note;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public StringBuffer getCote() {
        return cote;
    }

    public void setCote(String cote) {
        this.cote = new StringBuffer(cote);
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    public static String getNomMediatheque() {
        return nomMediatheque;
    }

    public static void setNomMediatheque(String nom) {
        nomMediatheque = nom;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException("Le clonage n'est pas supporté.");
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Media media = (Media) obj;
        return note == media.note &&
                titre.equals(media.titre) &&
                cote.toString().equals(media.cote.toString());
    }

    @Override
    public String toString() {
        return "Media{" +
                "titre='" + titre + '\'' +
                ", cote=" + cote +
                ", note=" + note +
                '}';
    }


}