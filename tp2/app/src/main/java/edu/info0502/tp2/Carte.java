package edu.info0502.tp2;

public class Carte {
    private String couleur;
    private String valeur;

    public Carte(String couleur, String valeur) {
        this.couleur = couleur;
        this.valeur = valeur;
    }

    public String getCouleur() {
        return couleur;
    }

    public String getValeur() {
        return valeur;
    }

    public int getVal() {
        switch (valeur) {
            case "2": return 2;
            case "3": return 3;
            case "4": return 4;
            case "5": return 5;
            case "6": return 6;
            case "7": return 7;
            case "8": return 8;
            case "9": return 9;
            case "10": return 10;
            case "Valet": return 11;
            case "Dame": return 12;
            case "Roi": return 13;
            case "As": return 14;
            default: return 0;
        }
    }

    @Override
    public String toString() {
        return valeur +" de "+ couleur;
    }
}
