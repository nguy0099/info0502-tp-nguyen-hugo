package edu.info0502.tp2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Talon {
    private List<Carte> cartes;

    public Talon() {
        cartes = new ArrayList<>();
    }

    public void ajouterPaquet(Paquet paquet) {
        cartes.addAll(paquet.getCartes());
    }

    public void melanger() {
        Collections.shuffle(cartes);
    }

    public Carte tirerCarte() {
        return cartes.size() > 0 ? cartes.remove(cartes.size() - 1) : null;
    }

    public List<Carte> getCartes() {
        return cartes;
    }

    public void afficherTalon() {
        for (Carte carte : cartes) {
            System.out.println(carte);
        }
    }
}
