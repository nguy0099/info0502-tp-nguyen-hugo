package edu.info0502.tp2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    private List<Carte> cartes;

    public Main() {
        this.cartes = new ArrayList<>(5);
    }

    public void ajouterCarte(Carte carte) {
        if (cartes.size() < 5) {
            cartes.add(carte);
        } else {
            System.out.println("Main déjà pleine !");
        }
    }

    public List<Carte> getCartes() {
        return cartes;
    }

    public void afficherMain() {
        System.out.println("Main : " + cartes);
    }

    public int evaluerMain() {
        trierMain();
        
        // Vérification des combinaisons de poker
        if (estQuinteFlushRoyale()) return 9;  // Meilleure main
        if (estQuinteFlush()) return 8;
        if (estCarre()) return 7;
        if (estFull()) return 6;
        if (estCouleur()) return 5;
        if (estSuite()) return 4;
        if (estBrelan()) return 3;
        int paires = compterPaires();
        if (paires == 2) return 2;  // Double Paire
        if (paires == 1) return 1;  // Paire

        return 0; // Aucune combinaison, retourne 0 pour la carte haute
    }

    private void trierMain() {
        Collections.sort(cartes, (c1, c2) -> Integer.compare(c1.getVal(), c2.getVal()));
    }

    private boolean estQuinteFlushRoyale() {
        return estCouleur() && estSuite() && cartes.get(4).getVal() == 14;
    }

    private boolean estQuinteFlush() {
        return estCouleur() && estSuite();
    }

    private boolean estCarre() {
        return compterOccurrences(4) > 0;
    }

    private boolean estFull() {
        return compterOccurrences(3) > 0 && compterOccurrences(2) > 0;
    }

    private boolean estCouleur() {
        String couleur = cartes.get(0).getCouleur();
        return cartes.stream().allMatch(carte -> carte.getCouleur().equals(couleur));
    }

    private boolean estSuite() {
        for (int i = 0; i < cartes.size() - 1; i++) {
            if (cartes.get(i).getVal() + 1 != cartes.get(i + 1).getVal()) return false;
        }
        return true;
    }

    private boolean estBrelan() {
        return compterOccurrences(3) > 0;
    }

    private int compterPaires() {
        return compterOccurrences(2);
    }

    private int compterOccurrences(int n) {
        int[] occurrences = new int[15];  // Pour les valeurs de 1 à 14 (les as)
        for (Carte carte : cartes) {
            occurrences[carte.getVal()]++;
        }
        int count = 0;
        for (int occ : occurrences) {
            if (occ == n) count++;
        }
        return count;
    }

    public String obtenirDescriptionMain() {
        trierMain();

        if (estQuinteFlushRoyale()) return "Quinte Flush Royale";
        if (estQuinteFlush()) return "Quinte Flush";
        if (estCarre()) return "Carré";
        if (estFull()) return "Full";
        if (estCouleur()) return "Couleur";
        if (estSuite()) return "Suite";
        if (estBrelan()) return "Brelan";
        int paires = compterPaires();
        if (paires == 2) return "Double Paire";
        if (paires == 1) return "Paire";

        return "Carte Haute : " + cartes.get(cartes.size() - 1).getVal();
    }
}
