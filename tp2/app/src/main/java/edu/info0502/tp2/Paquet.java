package edu.info0502.tp2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Paquet {
    private List<Carte> cartes;

    public Paquet() {
        this.cartes = new ArrayList<>();
        initialiserPaquet();
    }

    private void initialiserPaquet() {
        String[] couleurs = {"Coeur", "Carreau", "Trèfle", "Pique"};
        String[] valeurs = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "Valet", "Dame", "Roi", "As"};
        
        for (String couleur : couleurs) {
            for (String valeur : valeurs) {
                cartes.add(new Carte(couleur, valeur));
            }
        }
    }

    public void melanger() {
        Collections.shuffle(cartes);
    }

    public Carte tirerCarte() {
        if (!cartes.isEmpty()) {
            return cartes.remove(cartes.size() - 1);
        }
        return null;
    }

    public List<Carte> getCartes() {
        return cartes;
    }

    public int taille() {
        return cartes.size();
    }

    public void afficherPaquet() {
        for (Carte carte : cartes) {
            System.out.println(carte);
        }
    }
}
