package edu.info0502.tp2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class App {
    public String getGreeting() {
        String username = System.getProperty("user.name");
        return "Hello World, Hugo " + username + " ! Introduction à la programmation répartie !";
    }

    public static void main(String[] args) {
        System.out.println(new App().getGreeting());

        int nombreDeTours = 3; // Nombre de tours de jeu

        for (int tour = 1; tour <= nombreDeTours; tour++) {
            System.out.println("\n--- Tour " + tour + " ---");

            // Mise en place du talon
            Talon talon = new Talon();
            for (int i = 0; i < 2; i++) {
                Paquet paquet = new Paquet();
                paquet.melanger();
                talon.ajouterPaquet(paquet);
            }

            // Création de 4 joueurs avec leurs mains
            List<Joueur> joueurs = new ArrayList<>();
            for (int i = 1; i <= 4; i++) {
                joueurs.add(new Joueur("Joueur " + i));
            }

            // Distribution des cartes (5 cartes par joueur)
            for (int i = 0; i < 5; i++) {
                for (Joueur joueur : joueurs) {
                    Carte carte = talon.tirerCarte();
                    if (carte != null) {
                        joueur.recevoirCarte(carte);
                    }
                }
            }

            // Évaluation des mains
            System.out.println("\nÉvaluation des combinaisons obtenues :");
            List<Combo> classement = new ArrayList<>();  // Liste pour stocker le score et le joueur

            for (Joueur joueur : joueurs) {
                int score = joueur.getMain().evaluerMain();
                String combinaison = joueur.getMain().obtenirDescriptionMain();
                classement.add(new Combo(joueur.getNom(), combinaison, score));
                System.out.println(joueur.getNom() + " : " + combinaison + " (score : " + score + ")");
            }

            // Classement final
            System.out.println("\nClassement final :");
            Collections.sort(classement, Comparator.comparingInt(Combo::getScore).reversed()); // Tri par score décroissant

            for (int i = 0; i < classement.size(); i++) {
                Combo c = classement.get(i);
                System.out.println((i + 1) + ". " + c.getNom() + ": " + c.getCombinaison() + " (score : " + c.getScore() + ")");
            }
        }
    }
}

// Classe pour stocker le nom, la combinaison et le score d'un joueur
class Combo {
    private final String nom;
    private final String combinaison;
    private final int score;

    public Combo(String nom, String combinaison, int score) {
        this.nom = nom;
        this.combinaison = combinaison;
        this.score = score;
    }

    public String getNom() {
        return nom;
    }

    public String getCombinaison() {
        return combinaison;
    }

    public int getScore() {
        return score;
    }
}
