package edu.info0502.tp2;

public class Joueur {
    private String nom;
    private Main main;

    public Joueur(String nom) {
        this.nom = nom;
        this.main = new Main();
    }

    public String getNom() {
        return nom;
    }

    public Main getMain() {
        return main;
    }

    public void recevoirCarte(Carte carte) {
        main.ajouterCarte(carte);
    }

    @Override
    public String toString() {
        return nom +"a"+ main.toString();
    }
}
