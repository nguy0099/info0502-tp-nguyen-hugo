public class Film extends Media {
    private String realisateur;
    private int annee;

    // Constructeur par défaut
    public Film() {
        super(); 
        this.realisateur = "Anonyme";
        this.annee = 1950;
    }

    // Constructeur par initialisation
    public Film(String titre, String cote, int note, String realisateur, int annee) {
        super(titre, cote, note); 
        this.realisateur = realisateur;
        this.annee = annee;
    }

    // Constructeur par copie
    public Film(Film other) {
        super(other); 
        this.realisateur = other.realisateur;
        this.annee = other.annee;
    }

    // Getters et setters 
    public String getRealisateur() {
        return realisateur;
    }

    public void setRealisateur(String realisateur) {
        this.realisateur = realisateur;
    }

    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    // Masquage de la méthode toString
    @Override
    public String toString() {
        return super.toString() + ", realisateur='" + realisateur + '\'' +
                ", annee=" + annee;
    }
}
