import java.util.Vector;

public class Mediatheque {
    private String nomProprietaire;  
    private Vector<Media> medias;    

    public Mediatheque() {
        this.nomProprietaire = "";
        this.medias = new Vector<>(); 
    }

    public Mediatheque(Mediatheque other) {
        this.nomProprietaire = other.nomProprietaire;
        this.medias = new Vector<>(other.medias); 
    }

    public void add(Media media) {
        medias.add(media);
    }

    public String getNomProprietaire() {
        return nomProprietaire;
    }

    public void setNomProprietaire(String nomProprietaire) {
        this.nomProprietaire = nomProprietaire;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Mediatheque du propriétaire: ").append(nomProprietaire).append("\n");
        sb.append("Contient ").append(medias.size()).append(" médias :\n");

        for (Media media : medias) {
            sb.append(media.toString()).append("\n");
        }

        return sb.toString();
    }
}
